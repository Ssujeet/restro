import React from 'react';
import {Provider} from 'react-redux';
import {store} from './src/App/store';
import {Text} from 'react-native';

const App = () => {
  return (
    <Provider store={store}>
      <Text>Hello World</Text>
    </Provider>
  );
};

export default App;
