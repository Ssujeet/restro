import {configureStore} from '@reduxjs/toolkit';
import userReducer from '../Features/userSlice';

export const store = configureStore({
  reducer: {
    users: userReducer,
  },
  devTools: true,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
